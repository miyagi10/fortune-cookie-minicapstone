import React from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import { Table } from "reactstrap";

const QuoteRow = (props) => {
	console.log(props);

	const quote = props.quotes;

	return (
		<tr>
			<th scope="row">{props.index}</th>
			<td>{quote.category}</td>
			<td>{quote.description}</td>
			<td>
				<Button
					color="danger"
					className="mr-1"
					onClick={() => props.deleteQuote(quote._id)}>
					<i class="fas fa-trash"></i>
				</Button>
			</td>
		</tr>
	);
};

export default QuoteRow;
