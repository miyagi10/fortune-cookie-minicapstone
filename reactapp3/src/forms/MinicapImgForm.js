import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import { Fade } from "reactstrap";
import ReactDOM from "react-dom";
import {
	Button,
	Form,
	FormGroup,
	Input,
	Container,
	Row,
	Col,
	Card,
	CardImg,
	CardTitle,
	CardText,
	CardColumns,
	CardSubtitle,
	CardBody,
} from "reactstrap";

const MinicapImgForm = (props) => {
	//state
	const [minicapPic, setMinicapPic] = useState(undefined);

	const onChangeHandler = (e) => {
		setMinicapPic(e.target);
		setMinicapPic(e.target.files[0]);
	};

	const onSubmitHandler = (e) => {
		e.preventDefault();
		const formData = new FormData();
		formData.append("upload", minicapPic);
		props.updateImage(formData);
	};

	return (
		<Form
			className="border p-4 rounded mt-5"
			onSubmit={(e) => onSubmitHandler(e)}>
			<h1 align="center" id="title" style={{ paddingTop: "10px" }}>
				ASK THE FORTUNE COOKIE
			</h1>
			<Row style={{ padding: "10px" }}>
				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">LOVE</CardTitle>
						<Link to="/love">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328026278-stock-illustration-cute-fortune-cookie-on-a.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								What good fortune lies ahead for you and your special someone?.
								<br></br>
								<br></br>
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">CAREER</CardTitle>
						<Link to="/career">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328027464-stock-illustration-cartoon-fortune-cookie-isolated-in.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								Whether you're looking for work or going after promotion find
								out where your professional life is headed today!
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">HEALTH</CardTitle>
						<Link to="/health">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_334913354-stock-illustration-smiley-nurse-chinese-fortune-cookie.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								Learn about your personal health and get advice on how to feel
								and look your best today!
								<br></br>
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">MONEY</CardTitle>
						<Link to="/money">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328025902-stock-illustration-cute-fortune-cookie-character-smiley.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								Money can't buy happiness, but it sure does make life easier!
								<br></br>
								<br></br>
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</Form>
	);
};

export default MinicapImgForm;
