import React, { useState, useEffect, Fragment } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import AddQuoteForm from "../forms/AddQuoteForm";
import axios from "axios";
import AppNavbar from "../partials/AppNavbar";

const AddQuotePage = (props) => {
	return (
		<Fragment>
			<AppNavbar />
			<Container>
				<Row>
					<Col>
						<h1>Add Quote</h1>
					</Col>
				</Row>
				<Row>
					<Col>
						<AddQuoteForm />
					</Col>
				</Row>
			</Container>
		</Fragment>
	);
};

export default AddQuotePage;
