import React from "react";
import { Container, Row, Col, Button, Form } from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import CategoriesBtns from "../partials/CategoriesBtns";

const MoneyPage = (props) => {
	return (
		<Form className="border p-3 rounded m-5" id="container">
			<CategoriesBtns />
			<Row>
				<Col>
					<h1 id="title" style={{ paddingTop: "5px" }}>
						Money Cookie
					</h1>
					<h4>
						Money can't buy happiness, but it sure does make life easier! Just
						click on a cookie below.
					</h4>
				</Col>
			</Row>
			<Row xs="1" sm="2" md="4">
				<Col className="colImg">
					<Link
						to="/moneyCookie"
						style={{ background: "none", border: "none" }}>
						<img
							id="loveBtn"
							alt=""
							src="https://www.horoscope.com/images-US/games/game-fortune-cookie-1.png"></img>
					</Link>
				</Col>
				<Col className="colImg">
					<Link
						to="/moneyCookie"
						style={{ background: "none", border: "none" }}>
						<img
							id="loveBtn"
							alt=""
							src="https://www.horoscope.com/images-US/games/game-fortune-cookie-1.png"></img>
					</Link>
				</Col>
				<Col className="colImg">
					<Link
						to="/moneyCookie"
						style={{ background: "none", border: "none" }}>
						<img
							id="loveBtn"
							alt=""
							src="https://www.horoscope.com/images-US/games/game-fortune-cookie-1.png"></img>
					</Link>
				</Col>
			</Row>
		</Form>
	);
};

export default MoneyPage;
//test
