import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const NotFoundPage = (props) => {
	return (
		<Container className="my-5">
			<Row className="mb-3">
				<Col>
					<h1>Not Found Page</h1>
				</Col>
			</Row>
		</Container>
		)
}

export default NotFoundPage;