import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, ButtonGroup, Form } from "reactstrap";
import axios from "axios";

const CareerCookiePage = (props) => {
	console.log(props);

	const [quotesData, setQuotesData] = useState({
		quotes: [],
	});

	const { quotes } = quotesData;
	console.log(quotes);

	// const config = {
	// 	headers: {

	// 	}
	// }

	const url = "http://localhost:4000";

	const getCareerQuotes = async () => {
		try {
			const res = await axios.get(`${url}/quotes/randcareerquote`);
			console.log(res);
			setQuotesData({
				...quotesData,
				quotes: res.data,
			});
		} catch (e) {
			console.log(e);
			//SWAL
		}
	};

	useEffect(() => {
		getCareerQuotes();
	}, [setQuotesData]);

	return (
		<Form className="border p-3 rounded m-5" id="container">
			<h1 id="title" style={{ paddingTop: "30px", paddingLeft: "30px" }}>
				Your Career Cookie says....
			</h1>

			<div class="container">
				<img
					id="loveCookieImg"
					style={{ width: "60%" }}
					alt=""
					src="https://img.clipartlook.com/fortune-cookies-png-dixie-fortune-cookie-clip-art-640_456.png"></img>
				<div class="text-block centered" style={{ background: "white" }}>
					<p>{quotes}</p>
				</div>
				<div class="centered2" style={{ background: "white" }}></div>
			</div>
			<div class="container">
				<ButtonGroup class="Container">
					<Button className="mr-1" href="/">
						Categories
					</Button>
					<Button href="/career">Crack another...</Button>
				</ButtonGroup>
			</div>
		</Form>
	);
};

export default CareerCookiePage;

//testmrmbt
