import React, { useState, useEffect, Fragment } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import QuoteTable from "../tables/QuoteTable";
import axios from "axios";
import AppNavbar from "../partials/AppNavbar";

const MinicapPage = (props) => {
	console.log(props);

	const [quotesData, setQuotesData] = useState({
		quotes: [],
	});

	const { quotes } = quotesData;
	console.log(quotes);

	const config = {
		headers: {},
	};

	const url = "http://localhost:4000";

	const getQuotes = async (query = "") => {
		try {
			const res = await axios.get(`${url}/quotes/${query}`, config);
			console.log(res);
			setQuotesData({
				...quotesData,
				quotes: res.data,
			});
		} catch (e) {
			console.log(e);
			//SWAL
		}
	};

	const deleteQuote = async (id) => {
		try {
			const res = await axios.delete(`${url}/quotes/${id}`, config);
			console.log(res.data);

			getQuotes();
		} catch (e) {
			//swal
			console.log(e);
		}
	};

	useEffect(() => {
		getQuotes();
	}, [setQuotesData]);

	// console.log("Teamspage props", props.tokenAttr)

	// const [ teamsData, setTeamsData ] = useState({
	// 	token: props.tokenAttr,
	// 	teams: []
	// })

	// const { token, teams } = teamsData;
	// console.log("Teams Component", teams)

	// const getTeams = async () => {
	// 	try {
	// 		const config = {
	// 			headers : {
	// 				Authorization : `Bearer ${token}`
	// 			}

	// 		}

	// 		const res = await axios.get("http://localhost:4000/teams", config)

	// 		console.log("Teams Res", res)
	// 		setTeamsData({
	// 			teams: res.data
	// 		})

	// 	} catch(e) {
	// 		console.log(e.response)
	// 		//swal
	// 	}
	// }

	//getTeams()
	// useEffect(()=> {
	// 	getTeams()
	// }, [setTeamsData])

	return (
		<Fragment>
			<AppNavbar />
			<Container className="my-5">
				<Row className="mb-3">
					<Col>
						<h1>Landing page/Quotes Page</h1>
					</Col>
				</Row>
				<Row>
					{/* <Col md="4">
		        	<TeamForm/>
		        </Col> */}
					<Col>
						<Button className="btn-sm border mr-1" onClick={() => getQuotes()}>
							Get All
						</Button>
						<Button
							className="btn-sm border mr-1"
							onClick={() => getQuotes("?category=love")}>
							Love
						</Button>
						<Button
							className="btn-sm border mr-1"
							onClick={() => getQuotes("?category=career")}>
							Career
						</Button>
						<Button
							className="btn-sm border mr-1"
							onClick={() => getQuotes("?category=health")}>
							Health
						</Button>
						<Button
							className="btn-sm border mr-1"
							onClick={() => getQuotes("?category=money")}>
							Money
						</Button>

						<QuoteTable quotes={quotes} deleteQuote={deleteQuote} />
					</Col>
				</Row>
			</Container>
		</Fragment>
	);
};

export default MinicapPage;
