const Quote = require("../models/quotes");
const express = require("express");
const router = express.Router();
const sharp = require("sharp");
const random = require("mongoose-simple-random");
const multer = require("multer");
const upload = multer({
	// dest: "images/quotes",
	limits: { fileSize: 10000000 },
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(jpg|jpeg|png|PNG|img|svg)$/)) {
			return cb(new Error("Please upload an image only"));
		}
		cb(undefined, true);
	},
});

//Create a quote
router.post("/", async (req, res) => {
	const quote = new Quote(req.body);

	//save to db
	try {
		await quote.save();
		res.send(quote);
	} catch (e) {
		res.status(400).send(e);
	}
});

//GET ALL QUOTES
router.get("/", async (req, res) => {
	try {
		const quotes = await Quote.find(req.query);
		return res.status(200).send(quotes); //if no return, it will execute the succeeding lines
	} catch (e) {
		return res.status(404).send(e);
	}
});

router.post(
	"/upload/:id",
	upload.single("upload"),
	async (req, res) => {
		const buffer = await sharp(req.file.buffer)
			.resize({
				width: 100,
				height: 100,
			})
			.png()
			.toBuffer();

		const quote = await Quote.findById(req.params.id);
		quote.image = buffer;
		console.log(quote.image);
		await quote.save();
		res.send(quote);
		// res.send({ message: "Successfully uploaded image!"})
	},
	(error, req, res, next) => {
		res.status(400).send({ error: error.message });
	}
);

//DISPLAY A QUOTE
router.get("/:id/upload", async (req, res) => {
	try {
		const quote = await Quote.findById(req.params.id);
		console.log("55", quote);
		if (!quote || !quote.image) {
			return res.status(404).send("Quote Doesn't exist");
		}
		console.log(quote.image);
		// console.log("59", quote.image)
		//SEND BACK THE CORRECT DATA
		//TELL THE CLIENT WHAT TYPE OF DATA IT WILL RECEIVE
		res.set("Content-Type", "image/png");
		res.send(quote.image);
		console.log("64", quote.image);
	} catch (e) {
		res.status(500).send(e.message);
	}
});

//GET A RANDOM LOVE QUOTE
router.get("/randlovequote", async (req, res) => {
	try {
		const randlovequote = await Quote.aggregate([
			{ $match: { category: "love" } },
			{ $sample: { size: 1 } },
		]);
		// console.log(randlovequote)
		res.send(randlovequote[0].description);

		// console.log(randlovequote[])
	} catch (e) {
		console.log(e);
	}
});

//GET A RANDOM CAREER QUOTE
router.get("/randcareerquote", async (req, res) => {
	try {
		const randcareerquote = await Quote.aggregate([
			{ $match: { category: "career" } },
			{ $sample: { size: 1 } },
		]);
		res.send(randcareerquote[0].description);
	} catch (e) {
		console.log(e);
	}
});

//GET A RANDOM HEALTH QUOTE
router.get("/randhealthquote", async (req, res) => {
	try {
		const randhealthquote = await Quote.aggregate([
			{ $match: { category: "health" } },
			{ $sample: { size: 1 } },
		]);
		res.send(randhealthquote[0].description);
	} catch (e) {
		console.log(e);
	}
});

//GET A RANDOM MONEY QUOTE
router.get("/randmoneyquote", async (req, res) => {
	try {
		const randmoneyquote = await Quote.aggregate([
			{ $match: { category: "money" } },
			{ $sample: { size: 1 } },
		]);
		res.send(randmoneyquote[0].description);
	} catch (e) {
		console.log(e);
	}
});

//Delete quote
router.delete("/:id", async (req, res) => {
	//return res.send("delete a team");
	const _id = req.params.id;

	// Team.findByIdAndDelete(_id)
	// .then((team) => {
	// if(!team) {
	// return res.status(404).send(e)
	// }
	// return res.send(team)
	// })
	// .catch((e) => {
	// return res.status(500).send(e)
	// })
	try {
		const quote = await Quote.findByIdAndDelete(_id);
		if (!quote) {
			return res.status(404).send("Quote doesn't exist");
		}
		res.send(quote);
	} catch (e) {
		res.status(500).send(e.message);
	}
});

module.exports = router;
