const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const random = require("mongoose-simple-random");

const quoteSchema = new Schema(
	{
		category: {
			type: String,
			enum: ["love", "career", "money", "health"],
		},
		description: {
			type: String,
			maxlength: 300,
		},
		image: {
			type: Buffer,
			default: undefined,
		},
	},
	{
		timestamps: true,
	}
);

quoteSchema.plugin(random);
const Quote = mongoose.model("Quote", quoteSchema);
//Export the model
module.exports = Quote;
