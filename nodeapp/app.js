const express = require("express");
const app = express();
const mongoose = require("mongoose");
const multer = require("multer");
const cors = require("cors");

mongoose.connect("mongodb://localhost:27017/mini_capstone", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
});
mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});

//Apply Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//Declare Models
const Quote = require("./models/quotes");

//Create Routes/Endpoints
//Transferred Routes
//Declare the resources
const quotesRoute = require("./routes/quotes");
app.use("/quotes", quotesRoute);

//MULTER//
const upload = multer({
	dest: "images/quotes",
	limits: {
		fileSize: 100000, //max file size in bytes
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(jpg|jpeg|png|PNG)$/)) {
			return cb(new Error("Please upload an image only"));
		}

		cb(undefined, true);
	},
});
// Sample: Enpoint to upload a file
app.post(
	"/upload",
	upload.single("upload"),
	(req, res) => {
		res.send({ message: "Successfully uploaded image!" });
	},
	(error, req, res, next) => {
		res.status(400).send({ error: e.message });
	}
);
//MULTER//

//Initialize the server
app.listen(4000, () => {
	console.log("Now listening to port 4000");
});
